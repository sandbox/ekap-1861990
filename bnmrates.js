// $Id$

/**
* @file
* 
* The Javascript functions
* 
*/

/**
* Implements show/hide ov convertor
*/

function showHideConvertor() {
	if (jQuery("#bnm-rates-convertor").is(':visible')) {
		jQuery("#bnm-rates-convertor").hide();
		jQuery(".convertor-link a").html(Drupal.t('Show Convertor'));
	} else {

		jQuery("#bnm-rates-convertor").show();
		jQuery(".convertor-link a").html(Drupal.t('Hide Convertor'));
	}
}

/**
* Implements the convertion of the currencies
*/

function convertRates(obj) {
	jQuery('#MDL').val(Math.round(parseFloat(jQuery(obj).val() / currencies[jQuery(obj).attr('id')].eq) * 100) / 100);
	jQuery("#bnm-rates-convertor .form-text").each(function(idx) {
		if (jQuery(this).attr('id') != jQuery(obj).attr('id')) {
			jQuery(this).val(Math.round(parseFloat(currencies[jQuery(this).attr('id')].eq * jQuery('#MDL').val()) * 100) / 100);
		}
	});
}
